﻿/* [TODO]
 * - commencer par remplir les fonction Awake des deux scripte.
 * - compléter la fonction AddCube de ce script et l'action à faire lors de l'appuye sur la bare espace (UI manager)
 * - renseigner l'affichage sur l'UI globale pour avoir le nombre de cube à tous moment
 * - continuer sur l'ui manager pour finir les partie concernant le déplacement de la caméra.
 * - renseigner la partie concernant l'apparition de l'UI locale avec le clic droit
 * - renseigner la fonction ChangeColor et la lier au bouton correspondant dans l'UI locale
 * - renseigner la fonction PushTheCube et la lier au bouton correspondant dans l'UI locale
 * - renseigner la fonction MakeCubeExplose et la lier à un nouveau bouton dans l'UI locale
 * - modifier l'affichage des boutons pour qu'ils s'élargissent légèrement au survole
 * 				( penser à l'Animator)
 * */



using UnityEngine;
using System.Collections;

public class CubeManagerScript3GD : MonoBehaviour {
	
	public GameObject Prefab;				// doit etre renseigner avec le prefab du cube
	public float Force;						// intensité utilisée pour toutes les forces
	
	private UIManagerScript3GD UIManager;		// script gérant les interactions utilisateur
	private Transform TheLocalUI;			// UI qui apparait et disparait à coté des cubes
	private Transform TheCameraTransform;	// caméra
	private Transform TheTargetTransform;	// cible de la caméra
	
	// Use this for initialization
	void Awake () {
		/* [TODO]
		 * - créer une référence sur le script UIManager
		 * - créer une référence sur le transform de l'objet LocalUI
		 * - créer une référence sur le transform de la caméra
		 * - décommenter la référence sur le transform de la cible (target) de la camera
		 * */

		//TheTargetTransform = TheCameraTransform.GetComponent<OrbitCameraScript>()._target;
	}
	
	public void ChangeColor()
	{
		/* [TODO]
		 * - récupérer le MeshRenderer (appeller ensuite renderer) de l'objet parent de TheLocalUI
		 * - décommenter les modifications de couleurs
		 * */

		//renderer.material.SetColor("_Color", new Color(0f,1f,0f));
		//renderer.material.SetColor("_OutlineColor", new Color(0f,0.3f,0.1f));

		//NB: ces changement de couleur utilisent la notation interne du shader
	}
	
	public void PushTheCube()
	{
		/* [TODO]
		 * - récupérer l'objet parent de TheLocalUI
		 * - lui appliquer une force d'impulsion :
		 * 			- dans la direction de la profondeur de la caméra
		 * 			- d'intencité définie par le parametre Force
		 * */
	}
	
	public void MakeCubeExplose()
	{
		/* [TODO]
		 * - récupérer l'objet parent de TheLocalUI
		 * - le détruire
		 * - appliquer une force d'explosion (AddExplosionForce) de type impulsion:
		 * 			- à tous les objets contenu dans les 10m 
		 * 					- penser à la class Physics
		 * 					- penser aux boucles (for / foreach)
		 * 			- ajouter un biais pour propulser les cubes un peu plus vers le haut
		 * - desactiver le canvas (TheCanvas) afin de masquer l'UI locale.
		 * - décommenter la décrémentation du compteur
		 * */

		//UIManager.Counter--;
	}
	
	public void AddCube()
	{
		/* [TODO]
		 * - instancier le prefab (Prefab):
		 * 			- la position doit etre celle de la cible de la caméra
		 * 			- surelever légèrement cette position pour que les cubes ne se coincent pas dans le sol
		 * - décommenter l'uncrémentation du compteur
		 * */

		//UIManager.Counter++;
	}
}
