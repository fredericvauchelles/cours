﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManagerScript3GD : MonoBehaviour {
	
	public Transform LocalUI;				// UI qui apparait et disparait à coté des cubes
	public Transform GlobalUI;				// UI qui reste fixe à l'écran et affiche le nombre de cube
	public Camera TheCamera;				// la caméra
	public float TranslationSpeed = 25f;	// la vitesse de déplacement
	
	public int Counter 						// Propriété permettant de mettre à jour l'UI globale lors de l'acces à counter
	{	// property: comportement différent lors de la lecture et de l'écriture. 
		get //lecture
		{
			return this.counter;	// attention à la casse (majuscule)
		}
		set //écriture: value est la valeur que l'on va utiliser pour écrire
		{ 
			this.UpdateCounterText(value);	// appel d'une fonction de mise a jour de l'UI
			this.counter = value; 			// sauvegarde de la valeur
		}
	}
	public GameObject TheCanvas 			// le Canvas de l'UI locale
	{ 	// property: on peu changer la visibilité de l'écriture ou de la lecture indépendament
		get;			// lecture par defaut, accès autorizé à tous
		private set; 	// écriture par défault, accès restraint a ce script uniquement.
	}
	// Les propriété (Property en anglais) s'utilisent exactement comme des variables lors de leurs appels.
	
	private CubeManagerScript3GD TheCubeManager;	// le script gérant les cubes
	private GameObject TheCameraTarget;				// la cible de la caméra
	private OrbitCameraScript TheOrbitScript;		// script permetant d'avoir une caméra orbitale
	private Text TheTitre;							// titre de la "pop up" de l'UI locale
	private Text TheCounterText;					// texte affichant le nombre de cube (UI globale)
	private int counter;							//	nombre de cube dans la scene (si la scene commence vide de cube)
	
	
	// Use this for initialization
	void Awake () {
		/* [TODO]
		 * - créer une référence sur le script TheCubeManager
		 * - créer une référence sur le composant Text de l'objet Titre (LocalUI)
		 * 			- rappel : FindChild prend un chemin relatif à partir du transform où ont l'applique
		 * - créer une référence sur le composant Text de l'objet Counter (GlobalUI)
		 * - décommenter les lignes d'initialisation suivante
		 * */

		//TheCanvas = LocalUI.GetChild(0).gameObject;	// on récupère le premier enfant de LocalUI
		//TheOrbitScript = TheCamera.GetComponent<OrbitCameraScript>();
		//TheCameraTarget = TheOrbitScript._target.gameObject;
		//Counter = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(1))	// clic droit de la souris detecté
		{
			/* [TODO]
			 * - lancer un raycast:
			 * 			- si un cube est touché (indice: regarder les tag)
			 * 					- activer TheCanvas
			 * 					- changer la parenté de TheCanvas pour qu'il soit enfant de ce cube
			 * 					- changer la position de TheCanvas pour que le coin bas gauche de l'UI touche l'objet
			 * 					- changer la rotation de TheCanvas pour qu'il fasse toujours face à la caméra
			 * 					- changer le titre de la "pop up" pour qu'il prenne le nom du cube
			 * 			- sinon
			 * 					- désaactiver TheCanvas
			 * */
		}
		
		if (Input.GetKey(KeyCode.LeftArrow)) 
		{
			/* [TODO]
			 * - déplacer la cible de la caméra dans le monde sur la gauche du point de vue de la caméra
			 * - tenir compte de la vitesse de déplacement (donnée par seconde)
			 * - décommenter la ligne suivante pour mettre à jour la caméra
			 * */

			//TheOrbitScript.UpdateCameraFromOutside();
		}
		
		if (Input.GetKey(KeyCode.RightArrow)) 
		{
			/* [TODO]
			 * - déplacer la cible de la caméra dans le monde sur la droite du point de vue de la caméra
			 * - tenir compte de la vitesse de déplacement (donnée par seconde)
			 * - décommenter la ligne suivante pour mettre à jour la caméra
			 * */
			
			//TheOrbitScript.UpdateCameraFromOutside();
		}
		
		if (Input.GetKey(KeyCode.UpArrow)) 
		{
			/* [TODO]
			 * - déplacer la cible de la caméra dans le monde 
			 * 			- sur la profondeur (vers l'avant) du point de vue de la caméra
			 * 			- en restant parallele au sol (utiliser TheOrbitScript.GetXForRotation() 
			 * 					pour récupérer la rotation de la caméra autour de l'axe y)
			 * - tenir compte de la vitesse de déplacement (donnée par seconde)
			 * - décommenter la ligne suivante pour mettre à jour la caméra
			 * */
			
			//TheOrbitScript.UpdateCameraFromOutside();
		}
		
		if (Input.GetKey(KeyCode.DownArrow)) 
		{
			/* [TODO]
			 * - déplacer la cible de la caméra dans le monde 
			 * 			- sur la profondeur (vers l'arrière) du point de vue de la caméra
			 * 			- en restant parallele au sol (utiliser TheOrbitScript.GetXForRotation() 
			 * 					pour récupérer la rotation de la caméra autour de l'axe y)
			 * - tenir compte de la vitesse de déplacement (donnée par seconde)
			 * - décommenter la ligne suivante pour mettre à jour la caméra
			 * */
			
			//TheOrbitScript.UpdateCameraFromOutside();
		}
		
		if (Input.GetKey (KeyCode.Space)) 
		{
			/* [TODO]
			 * - ajouter un cube (voir les fonction du CubeManagerScript3GD
			 * */
		}
		
		//block rotation of the menu when the object move
		Transform TheParentTransform = LocalUI.parent;
		if (TheParentTransform)
		{
			Rigidbody TheParentRigidbody = TheParentTransform.gameObject.GetComponent<Rigidbody>();
			if(Input.GetButton("Fire1") || (TheParentRigidbody && (TheParentRigidbody.angularVelocity.magnitude>0.1f || TheParentRigidbody.velocity.magnitude>0.1f)))
			{				
				/* [TODO]
				 * Soit la caméra tourne, soit l'objet bouge. Il faut donc s'assurer ici que l'UI locale
				 * 		soit toujours face à la caméra.
				 * */
			}
		}
	}
	
	void UpdateCounterText(int number)
	{
		/* [TODO]
		 * - changer le text du composant TheCounterText pour que l'affichage en haut à gauche
		 * 			dans l'UI se mette à jour.
		 * - différencier les cas 0 cube, 1 cube et plusieurs cubes :
		 * 			"Il n'y a pas de cube dans la scène."
		 * 			"Il y a 1 cube dans la scène."
		 * 			"Il y X cubes dans la scène." (X a renseigner)
		 * */
	}
}
