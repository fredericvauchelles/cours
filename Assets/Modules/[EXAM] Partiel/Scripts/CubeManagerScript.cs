﻿using UnityEngine;
using System.Collections;

public class CubeManagerScript : MonoBehaviour {

	public GameObject Prefab;
	public float Force;

	private UIManagerScript UIManager;
	private Transform TheLocalUI;
	private Transform TheCameraTransform;
	private Transform TheTargetTransform;

	// Use this for initialization
	void Awake () {
		UIManager = GetComponent<UIManagerScript>();
		TheLocalUI = UIManager.LocalUI;
		TheCameraTransform = UIManager.TheCamera.transform;
		TheTargetTransform = TheCameraTransform.GetComponent<OrbitCameraScript>()._target;
	}

	public void ChangeColor()
	{
		GameObject Target = TheLocalUI.parent.gameObject;
		MeshRenderer renderer = Target.GetComponent<MeshRenderer>();
		renderer.material.SetColor("_Color", new Color(0f,1f,0f));
		renderer.material.SetColor("_OutlineColor", new Color(0f,0.3f,0.1f));
	}

	public void PushTheCube()
	{
		Rigidbody Target = TheLocalUI.parent.gameObject.GetComponent<Rigidbody>();;
		Target.AddForce(TheCameraTransform.forward * Force, ForceMode.Impulse);
	}

	public void MakeCubeExplose()
	{
		GameObject CubeToDestroy = TheLocalUI.parent.gameObject;
		TheLocalUI.SetParent(null);
		Vector3 explosionPos = CubeToDestroy.transform.position;
		GameObject.Destroy (CubeToDestroy);
		Collider[] colliders = Physics.OverlapSphere(explosionPos, Force);
		foreach (Collider hit in colliders) 
		{
			if (hit && hit.attachedRigidbody)
			{
				hit.attachedRigidbody.AddExplosionForce(Force, explosionPos, Force, 3.0F, ForceMode.Impulse);
			}			
		}
		UIManager.TheCanvas.SetActive(false);
		UIManager.Counter--;
	}

	public void AddCube()
	{
		GameObject.Instantiate(Prefab, TheTargetTransform.position + Vector3.up, Quaternion.identity);
		UIManager.Counter++;
	}
}
