﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManagerScript : MonoBehaviour {
	
	public Transform LocalUI;
	public Transform GlobalUI;
	public Camera TheCamera;
	public float TranslationSpeed = 25f;

	public int Counter 
	{	// property: comportement différent lors de la lecture et de l'écriture. 
		get //lecture
		{
			return this.counter;	// attention à la casse (majuscule)
		}
		set //écriture: value est la valeur que l'on va utiliser pour écrire
		{ 
			this.UpdateCounterText(value);	// appel d'une fonction de mise a jour de l'UI
			this.counter = value; 			// sauvegarde de la valeur
		}
	}
	public GameObject TheCanvas 
	{ 	// property: on peu changer la visibilité de l'écriture ou de la lecture indépendament
		get;			// lecture par defaut, accès autorizé à tous
		private set; 	// écriture par défault, accès restraint a ce script uniquement.
	}
	// Les propriété (Property en anglais) s'utilisent exactement comme des variables lors de leurs appels.

	private CubeManagerScript TheCubeManager;
	private GameObject TheCameraTarget;
	private OrbitCameraScript TheOrbitScript;
	private Text TheTitre;
	private Text TheCounterText;
	private int counter;


	// Use this for initialization
	void Awake () {
		TheCubeManager = GetComponent<CubeManagerScript>();
		TheCanvas = LocalUI.GetChild(0).gameObject;
		TheTitre = TheCanvas.transform.FindChild("Panel/Titre").GetComponent<Text>();
		TheOrbitScript = TheCamera.GetComponent<OrbitCameraScript>();
		TheCameraTarget = TheOrbitScript._target.gameObject;
		TheCounterText = GlobalUI.FindChild("Panel/Counter").GetComponent<Text>();
		Counter = 0;
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(1))
		{
			Ray ray = TheCamera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, 100) && hit.rigidbody && hit.rigidbody.tag == "Cube")
			{
				TheCanvas.SetActive(true);
				LocalUI.SetParent(hit.transform);
				LocalUI.localPosition = Vector3.zero;
				LocalUI.rotation = TheCamera.transform.rotation;
				TheTitre.text = hit.transform.name;
			}
			else
			{
				TheCanvas.SetActive(false);
			}
		}
		
		if (Input.GetKey(KeyCode.LeftArrow)) 
		{
			TheCameraTarget.transform.Translate(TheCamera.transform.TransformDirection(Vector3.left) * (Time.deltaTime * TranslationSpeed));
			TheOrbitScript.UpdateCameraFromOutside();
		}
		
		if (Input.GetKey(KeyCode.RightArrow)) 
		{
			TheCameraTarget.transform.Translate(TheCamera.transform.TransformDirection(Vector3.right) * (Time.deltaTime * TranslationSpeed));
			TheOrbitScript.UpdateCameraFromOutside();
		}
		
		if (Input.GetKey(KeyCode.UpArrow)) 
		{
			TheCameraTarget.transform.Translate(Quaternion.Euler(0f,TheOrbitScript.GetXForRotation(),0f) * Vector3.forward * (Time.deltaTime * TranslationSpeed));
			TheOrbitScript.UpdateCameraFromOutside();
		}
		
		if (Input.GetKey(KeyCode.DownArrow)) 
		{
			TheCameraTarget.transform.Translate(Quaternion.Euler(0f,TheOrbitScript.GetXForRotation(),0f) * Vector3.back * (Time.deltaTime * TranslationSpeed));
			TheOrbitScript.UpdateCameraFromOutside();
		}

		if (Input.GetKey (KeyCode.Space)) 
		{
			TheCubeManager.AddCube();
		}

		//block rotation of the menu when the object move
		Transform TheParentTransform = LocalUI.parent;
		if (TheParentTransform)
		{
			Rigidbody TheParentRigidbody = TheParentTransform.gameObject.GetComponent<Rigidbody>();
			if(Input.GetButton("Fire1") || (TheParentRigidbody && (TheParentRigidbody.angularVelocity.magnitude>0.1f || TheParentRigidbody.velocity.magnitude>0.1f)))
			{				
				LocalUI.rotation = TheCamera.transform.rotation;
			}
		}
	}

	void UpdateCounterText(int number)
	{
		switch (number) 
		{
		case 0:
			TheCounterText.text = "Il n'y a pas de cube dans la scène.";
			break;
		case 1:
			TheCounterText.text = "Il y a 1 cube dans la scène.";
			break;
		default:
			TheCounterText.text = "Il y " + number.ToString () + " cubes dans la scène.";
			break;
		}
	}
}
