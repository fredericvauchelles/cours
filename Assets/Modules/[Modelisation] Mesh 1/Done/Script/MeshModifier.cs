﻿using UnityEngine;
using System.Collections;

public class MeshModifier : MonoBehaviour {

	public Mesh TheMesh;
	public MeshFilter TheMeshFilter;
	public int VertexToMove;
	public int VertexToSwap1;
	public int VertexToSwap2;

	private float timeSpendFromStartDeploying = 0f;
	private bool bDeploying = false;
	private bool bDeployingVertex = false;
	private bool bDeployed = false;
	private bool bSwapingVertices = false;
	
	private Vector3[] TheVertices;
	private int[] TheTriangles;
	private Vector3[] TheNormals;
	private Vector4[] TheTangents;

	// Use this for initialization
	void Start () {
		TheMesh = TheMeshFilter.mesh;
		Debug.Log ("=====Vertices in TheMesh=====");
		TheVertices = TheMesh.vertices;
		Debug.Log ("SIZE=" + TheVertices.Length);
		foreach (Vector3 vertice in TheVertices) {
			Debug.Log(vertice);
		}
		Debug.Log ("=====Triangles in TheMesh====");
		TheTriangles = TheMesh.triangles;
		Debug.Log ("SIZE=" + TheTriangles.Length);
		foreach (int triangle in TheTriangles) {
			Debug.Log(triangle);
		}
		Debug.Log ("=====Normals in TheMesh======");
		TheNormals = TheMesh.normals;
		Debug.Log ("SIZE=" + TheNormals.Length);
		foreach (Vector3 normal in TheNormals) {
			Debug.Log (normal);
		}
		Debug.Log ("=====Tangents in TheMesh=====");
		TheTangents = TheMesh.tangents;
		Debug.Log ("SIZE=" + TheTangents.Length);
		foreach (Vector4 tangent in TheTangents) {
			Debug.Log (tangent);
		}
		Debug.Log ("=============================");
	}
	
	// Update is called once per frame
	void Update() {
		if (bDeploying)
			Deploying();
		if (bDeployingVertex) 
			DeployingVertex(VertexToMove);
		if (bSwapingVertices)
			SwapingVertices (VertexToSwap1, VertexToSwap2);
	}

	//Draw harsh GUI
	void OnGUI(){
		int y = -50;
		if (GUI.Button (new Rect(10,y+=60,200,50), "Go Through Normals") && !bDeployed) {
			bDeploying = bDeployed = true;
		}
		if (GUI.Button (new Rect(10,y+=60,200,50), "Move Vertex " + VertexToMove) && !bDeployed) {
			bDeployingVertex = bDeployed = true;
		}
		if (GUI.Button (new Rect(10,y+=60,200,50), "Swap Vertice " + VertexToSwap1 + " " + VertexToSwap2) && !bDeployed) {
			bSwapingVertices = bDeployed = true;
		}
		if (GUI.Button (new Rect(10,y+=60,200,50), "Clear") && bDeployed) {
			Clear ();
		}
	}

	void Clear(){
		TheMesh.vertices = TheVertices;
		TheMesh.triangles = TheTriangles;
		TheMesh.normals = TheNormals;
		TheMesh.tangents = TheTangents;
		bDeployed = bDeployingVertex = bDeployingVertex = false;
	}
	
	void Deploying(){
		Vector3[] vertices = TheMesh.vertices;
		Vector3[] normals = TheMesh.normals;
		int i = 0;
		while (i < vertices.Length) {
			vertices[i] += normals[i] * Mathf.Sin(timeSpendFromStartDeploying * 0.05f) * 0.05f;
			++i;
		}
		TheMesh.vertices = vertices;
		timeSpendFromStartDeploying += Time.deltaTime;
		TheMesh.RecalculateBounds ();
		TheMesh.RecalculateNormals ();
		
		if (timeSpendFromStartDeploying > 1f){
			bDeploying = false;
			timeSpendFromStartDeploying = 0f;
		}
	}

	void DeployingVertex(int i){
		Vector3[] vertices = TheMesh.vertices;
		Vector3[] normals = TheMesh.normals;
		vertices[i] += normals[i] * Mathf.Sin(timeSpendFromStartDeploying * 0.25f) * 0.2f;
		TheMesh.vertices = vertices;
		timeSpendFromStartDeploying += Time.deltaTime;
		TheMesh.RecalculateBounds ();
		TheMesh.RecalculateNormals ();
		
		if (timeSpendFromStartDeploying > 1f){
			bDeployingVertex = false;
			timeSpendFromStartDeploying = 0f;
		}
	}

	void SwapingVertices (int Vertex1, int Vertex2){
		Vector3[] vertices = TheMesh.vertices;
		Vector3[] normals = TheMesh.normals;
		vertices [Vertex1] = Vector3.Lerp (TheVertices [Vertex1], TheVertices [Vertex2], timeSpendFromStartDeploying);
		vertices [Vertex2] = Vector3.Lerp (TheVertices [Vertex2], TheVertices [Vertex1], timeSpendFromStartDeploying);
		TheMesh.vertices = vertices;
		timeSpendFromStartDeploying += Time.deltaTime;
		TheMesh.RecalculateBounds ();
		TheMesh.RecalculateNormals ();

		if (timeSpendFromStartDeploying > 1f){
			bSwapingVertices = false;
			timeSpendFromStartDeploying = 0f;
		}
	}
}
