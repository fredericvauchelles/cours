﻿using UnityEngine;
using System.Collections;
//using System;

public class NetworkManager : MonoBehaviour {

	private const string roomName = "RoomName";
	private RoomInfo[] roomsList;

	public GameObject playerPrefab;

	// Use this for initialization
	void Start () {
		PhotonNetwork.ConnectUsingSettings("0.1");	//0.1 is the number version of this game !
	}
	
	void OnGUI()
	{
		if (!PhotonNetwork.connected)	//not connected to the PhotonNetwork
		{
			GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
		}
		else if (PhotonNetwork.room == null)	//connected and not in a room
		{
			// Create Room
			if (GUI.Button(new Rect(100, 100, 250, 100), "Start Server"))	//add a button for room creation
				PhotonNetwork.CreateRoom(roomName + System.Guid.NewGuid().ToString("N"), true, true, 5);
			
			// Join Room
			if (roomsList != null)	//if rooms exist, display them
			{
				for (int i = 0; i < roomsList.Length; i++)
				{
					//display them as button in order to connect to them
					if (GUI.Button(new Rect(100, 250 + (110 * i), 250, 100), "Join " + roomsList[i].name))	
						PhotonNetwork.JoinRoom(roomsList[i].name);
				}
			}
		}
	}
	
	void OnReceivedRoomListUpdate()	//called each time the room list change
	{
		roomsList = PhotonNetwork.GetRoomList();
	}

	void OnJoinedRoom()	//called when joining a room
	{
		// Spawn player
		PhotonNetwork.Instantiate(playerPrefab.name, Vector3.up * 2, Quaternion.identity, 0);
		//Instantiate(playerPrefab,Vector3.up*5,Quaternion.identity);
	}
}
