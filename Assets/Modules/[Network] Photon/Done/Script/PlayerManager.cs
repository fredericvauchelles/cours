﻿using UnityEngine;
using System.Collections;

public class PlayerManager : Photon.MonoBehaviour {

	public float speed = 10f;
	
	void Update()
	{
		if (photonView.isMine)	// Only for the Player that have been instanciated in our view
		{
			InputMovement ();
			InputColorChange ();
		}
		else
			SyncedMovement();
	}
	
	void InputMovement()	// Our interaction listener
	{
		if (Input.GetKey(KeyCode.Z))
			GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + Vector3.forward * speed * Time.deltaTime);
		
		if (Input.GetKey(KeyCode.S))
			GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position - Vector3.forward * speed * Time.deltaTime);
		
		if (Input.GetKey(KeyCode.D))
			GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + Vector3.right * speed * Time.deltaTime);
		
		if (Input.GetKey(KeyCode.Q))
			GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position - Vector3.right * speed * Time.deltaTime);
	}


	/*
	 * Syncronization !
	 */ 
	private float lastSynchronizationTime = 0f;
	private float syncDelay = 0f;
	private float syncTime = 0f;
	private Vector3 syncStartPosition = Vector3.zero;
	private Vector3 syncEndPosition = Vector3.zero;

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)	//state synchronization
	{
		if (stream.isWriting) {
			stream.SendNext (GetComponent<Rigidbody>().position);
			stream.SendNext (GetComponent<Rigidbody>().velocity);
		}
		else 
		{
			/*syncEndPosition = (Vector3)stream.ReceiveNext ();
			syncStartPosition = GetComponent<Rigidbody>().position;
			
			syncTime = 0f;
			syncDelay = Time.time - lastSynchronizationTime;
			lastSynchronizationTime = Time.time;*/


			Vector3 syncPosition = (Vector3)stream.ReceiveNext();
			Vector3 syncVelocity = (Vector3)stream.ReceiveNext();
			
			syncTime = 0f;
			syncDelay = Time.time - lastSynchronizationTime;
			lastSynchronizationTime = Time.time;
			
			syncEndPosition = syncPosition + syncVelocity * syncDelay;
			syncStartPosition = GetComponent<Rigidbody>().position;
		}
	}

	void SyncedMovement()
	{
		syncTime += Time.deltaTime;
		GetComponent<Rigidbody>().position = Vector3.Lerp(syncStartPosition, syncEndPosition, syncTime / syncDelay);
	}

	/*
	 * RPC
	 */

	private void InputColorChange()
	{
		if (Input.GetKeyDown(KeyCode.R))
			ChangeColorTo(new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)));
	}

	[RPC] void ChangeColorTo(Vector3 color)
	{
		GetComponent<MeshRenderer>().material.color = new Color(color.x, color.y, color.z, 1f);
		
		if (photonView.isMine)
			photonView.RPC("ChangeColorTo", PhotonTargets.OthersBuffered, color);
	}
}
