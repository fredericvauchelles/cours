﻿namespace PhysiqueN1
{
    using UnityEngine;

    public class Done_02_DestroyCollision : MonoBehaviour
    {
        // Sur une collision
        void OnCollisionEnter()
        {
            // se détruire soi
            Destroy(gameObject);
        }
    }
}
