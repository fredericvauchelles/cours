﻿namespace PhysiqueN1
{
    using UnityEngine;

    public class Done_03_DestroyCollisionTagged : MonoBehaviour
    {
        [SerializeField]
        string m_Tag = string.Empty;

        void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == m_Tag)
            {
                Destroy(gameObject);
            }
        }
    }
}
