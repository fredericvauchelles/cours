﻿namespace PhysiqueN1
{
    using UnityEngine;

    public class Done_05_DestroyTriggerExit : MonoBehaviour
    {
        void OnTriggerExit(Collider other)
        {
            Destroy(other.gameObject);
        }
    }
}
