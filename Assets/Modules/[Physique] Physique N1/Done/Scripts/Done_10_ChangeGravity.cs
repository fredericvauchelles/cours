﻿namespace PhysiqueN1
{
    using System.Collections;
    using UnityEngine;

    public class Done_10_ChangeGravity : MonoBehaviour
    {
        [Header("Gravity Change")]
        [SerializeField]
        float m_UpdateDelay = 4;
        [SerializeField]
        float m_Damping = 3;
        [SerializeField]
        float m_GravityMin = 9;
        [SerializeField]
        float m_GravityMax = 10;

        void Awake()
        {
            // On intialise avec la gravité actuelle
            m_TargetGravity = Physics.gravity;
        }

        void OnEnable()
        {
            // Démarrage de la coroutine pour mettre à jour la gravité cible
            m_UpdateGravity_Coroutine = UpdateGravity();
            StartCoroutine(m_UpdateGravity_Coroutine);
        }

        void OnDisable()
        {
            // Fin de la coroutine
            StopCoroutine(m_UpdateGravity_Coroutine);
        }

        void FixedUpdate()
        {
            // On interpole la gravité à chaque mise à jour physique
            Physics.gravity = Vector3.Lerp(Physics.gravity, m_TargetGravity, Time.fixedDeltaTime * m_Damping);
        }

        IEnumerator UpdateGravity()
        {
            while (enabled)
            {
                // Calcul de la nouvelle gravité cible
                // Amplitude aléatoire
                var magnitude = Random.Range(m_GravityMin, m_GravityMax);
                // Direction aléatoire, de norme 1
                var direction = Random.insideUnitSphere.normalized;
                // Nouvelle gravité cible
                m_TargetGravity = direction * magnitude;

                // On attend la mise à jour suivante
                yield return new WaitForSeconds(m_UpdateDelay);
            }
        }

        Vector3 m_TargetGravity = Vector3.zero;
        IEnumerator m_UpdateGravity_Coroutine = null;
    }
}
