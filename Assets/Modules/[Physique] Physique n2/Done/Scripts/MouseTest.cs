﻿using UnityEngine;
using System.Collections;

public class MouseTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			Vector3 MouthPos = Input.mousePosition;
			print ("Mouth button pressed at " + MouthPos);
			RaycastHit hit = new RaycastHit();
			Ray ray = Camera.main.ScreenPointToRay(MouthPos);
			if(Physics.Raycast(ray, out hit))
			{
				print ("touché!");
			}
		}
	}
}
