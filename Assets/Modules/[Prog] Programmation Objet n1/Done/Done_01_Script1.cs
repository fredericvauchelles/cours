﻿namespace ProgrammationObjetN1
{
    using UnityEngine;

    public class Done_01_Script1 : MonoBehaviour
    {
        public Done_01_Script2 theOtherScript = null;
        public int myInteger = 0;
        public string myString = string.Empty;

        private int m_MyHiddenInteger = 0;
        
        void Start()
        {
            // Attribut publique
            theOtherScript.myBool = true;

            // Méthode publique
            var getAFloat = theOtherScript.MyService();

            // Invalide car attribut privé
            //theOtherScript.m_MyHiddenFloat = 2;
        }
    }
}