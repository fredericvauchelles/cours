﻿namespace ProgrammationObjetN1
{
    using UnityEngine;

    public class Done_01_Script2 : MonoBehaviour
    {
        public bool myBool = false;
        public string myString = string.Empty;

        public float MyService()
        {
            return m_MyHiddenFloat;
        }

        private float m_MyHiddenFloat = 0;
        private Done_01_Script1 m_TheOtherScript = null;
    }
}