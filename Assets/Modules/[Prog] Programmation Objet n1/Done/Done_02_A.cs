﻿namespace ProgrammationObjetN1
{
    using UnityEngine;

    public class Done_02_A : MonoBehaviour
    {
        virtual public void SayHello(string name)
        {
            Debug.Log("Hello " + name);
        }
    }
}