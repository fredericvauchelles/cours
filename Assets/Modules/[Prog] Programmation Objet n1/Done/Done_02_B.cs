﻿namespace ProgrammationObjetN1
{
    using UnityEngine;

    public class Done_02_B : Done_02_A
    {
        public override void SayHello(string name)
        {
            // Appel du parent
            base.SayHello(name);

            Debug.Log("From B !");
        }
    }
}