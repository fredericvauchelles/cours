﻿namespace ProgrammationObjetN1
{
    using UnityEngine;

    public class Done_02_C : Done_02_A
    {
        public override void SayHello(string name)
        {
            // On a remplacé entièrement la méthode sans appeller le parent 'base.SayHello()'
            Debug.Log("Say Just Hello to " + name);
        }
    }
}