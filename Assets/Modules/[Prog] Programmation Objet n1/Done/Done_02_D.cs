﻿namespace ProgrammationObjetN1
{
    using UnityEngine;

    public class Done_02_D : MonoBehaviour
    {
        // On utilise SerializeField pour pouvoir afficher un attribut privé dans l'inspecteur Unity
        // L'attribut étant privé, il ne sera pas accessible par une autre classe
        // Mais nous, on peut le configurer dans l'inspecteur :)
        [SerializeField]
        Done_02_A m_A = null;
        [SerializeField]
        Done_02_B m_B = null;
        [SerializeField]
        Done_02_C m_C = null;

        void Start()
        {
            m_A.SayHello("To Me");
            m_B.SayHello("To Me");
            m_C.SayHello("To Me");
        }
    }
}