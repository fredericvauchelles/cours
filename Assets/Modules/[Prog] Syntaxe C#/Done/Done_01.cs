﻿// Namespace permettant d'éviter les collisions des noms des classes
namespace CSharpSyntax
{
    // Import des bibliothèques
    using UnityEngine;

    public class Done_01 : MonoBehaviour
    {
        void Start()
        {
            // Variables
            int argument1 = 1;
            float argument2 = 2.0f;
            
            // Appel de méthode
            float added = Add(argument1, argument2);

            // Affichage du résultat
            Debug.Log("Done_01.Add(" + argument1 + "," + argument2 + ") = " + added);
        }

        float Add(float left, float right)
        {
            return left + right;
        }
    }
}