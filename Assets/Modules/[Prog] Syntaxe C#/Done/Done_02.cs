﻿// Namespace permettant d'éviter les collisions des noms des classes
namespace CSharpSyntax
{
    // Import des bibliothèques
    using UnityEngine;

    public class Done_02 : MonoBehaviour
    {
        void Start()
        {
            // Variables
            int argument1 = 1;
            float argument2 = 2.0f;
            var argument3 = 4.0f;

            // Appel de méthode
            float result = Product(argument1, argument2, argument3);

            // Affichage du résultat
            Debug.Log("Done_02.Product(" + argument1 + "," + argument2 + "," + argument3 + ") = " + result);
        }

        float Product(float left, float mid, float right)
        {
            return left * mid * right;
        }
    }
}