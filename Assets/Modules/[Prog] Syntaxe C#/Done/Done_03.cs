﻿// Namespace permettant d'éviter les collisions des noms des classes
namespace CSharpSyntax
{
    // Import des bibliothèques
    using UnityEngine;

    public class Done_03 : MonoBehaviour
    {
        void Start()
        {
            // Variables
            var left = "Hello";
            var right = " World!";

            // Appel de méthode
            string result = Concat(left, right);

            // Affichage du résultat
            Debug.Log("Done_03.Concat(" + left + "," + right + ") = " + result);
        }

        string Concat(string left, string right)
        {
            return left + right;
        }
    }
}