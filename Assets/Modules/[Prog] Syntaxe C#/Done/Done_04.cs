﻿// Namespace permettant d'éviter les collisions des noms des classes
namespace CSharpSyntax
{
    // Import des bibliothèques
    using UnityEngine;

    public class Done_04 : MonoBehaviour
    {
        void Start()
        {
            // Variables
            var integers = new int[] { 1, 4, 2, 7, 3, 1 };

            // Appel de méthode
            var result = Sum(integers);

            // Affichage du résultat
            Debug.Log("Done_04.Sum(integers) = " + result);
        }

        int Sum(int[] ints)
        {
            // Initialisation de la somme
            var count = 0;
            // On parcours le tableau par son indice
            for (int i = 0; i < ints.Length; i++)
            {
                // On ajoute à 'count' la valeur du tableau à l'index 'i' (c'est à dire 'ints[i]')
                count += ints[i];
            }
            return count;
        }
    }
}