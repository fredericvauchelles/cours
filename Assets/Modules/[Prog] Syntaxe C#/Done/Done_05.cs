﻿// Namespace permettant d'éviter les collisions des noms des classes
namespace CSharpSyntax
{
    // Import des bibliothèques
    using UnityEngine;

    public class Done_05 : MonoBehaviour
    {
        void Start()
        {
            // Variables
            var integers = new int[] { 1, 4, 2, 7, 3, 1 };

            // Appel de méthode
            var average = AddOrAverage(integers, true);
            var sum = AddOrAverage(integers, false);

            // Affichage du résultat
            Debug.Log("Done_05.AddOrAverage(integers, true) = " + average);
            Debug.Log("Done_05.AddOrAverage(integers, false) = " + sum);
        }

        float AddOrAverage(int[] ints, bool doAverage)
        {
            // Initialisation de la somme
            var count = 0.0f;
            // On parcours le tableau par son indice
            for (int i = 0; i < ints.Length; i++)
            {
                // On ajoute à 'count' la valeur du tableau à l'index 'i' (c'est à dire 'ints[i]')
                count += ints[i];
            }

            // Si on veut faire la moyenne et qu'il y a au moins un élément
            if (doAverage && ints.Length > 0)
            {
                // On divise par le nombre d'éléments
                // On divise bien par un flotant ici, pour éviter une division entre nombres entiers et avoir la moyenne
                count /= ints.Length;
            }

            return count;
        }
    }
}