﻿using UnityEngine;
using System.Collections;

public class GizmoTest : MonoBehaviour {

	void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Gizmos.DrawSphere(transform.position, 0.5f);
	}	

	void OnDrawGizmosSelected () {
		// Display the explosion radius when selected
		Gizmos.color = Color.green;
		Gizmos.DrawSphere(transform.position, 0.5f);
	}

	void Update() {
		Debug.DrawLine(Vector3.zero, new Vector3(0, 3, 0), Color.red);
	}
}
