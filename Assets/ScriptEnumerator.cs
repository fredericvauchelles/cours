﻿using UnityEngine;
using System.Collections;

public class ScriptEnumerator : MonoBehaviour {

	private int FrameNumber;

	int i;

	// Use this for initialization
	void Awake() {
		FrameNumber = 0;
	}

	void Start()
	{
		StartCoroutine(Count0To10());
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log ("Frame n°" + FrameNumber.ToString() + " i=" + i.ToString());
		//FrameNumber ++;		
		//StartCoroutine("Count0To10");

	}

	IEnumerator Count0To10()
	{
		try
		{
			for (i = 0; i<=10; ++i) 
			{
				Debug.Log ("i=" + i.ToString());
				yield return new WaitForSeconds(1f);
			}
		}
		finally
		{
			Debug.Log("Finished !");
		}
	}
}
